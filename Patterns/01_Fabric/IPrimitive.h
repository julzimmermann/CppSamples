#pragma once
class IPrimitive
{
public:
	virtual ~IPrimitive() = default; // virtual for delete override

	virtual void Draw() = 0;
	virtual void setWidth(int widthVal) = 0;
	virtual void setHeight(int heightVal) = 0;
	virtual void setLeftUpper(int X, int Y) = 0;
	virtual void setRect(int X, int Y, int widthVal, int heightVal) = 0;
};

class APrimitive : public IPrimitive
{
public:
	APrimitive() : x{ 0 }, y{ 0 }, width{ 0 }, height{ 0 } {};
	~APrimitive() override {}

	void setWidth(int widthVal) override;
	void setHeight(int heightVal) override ;
	void setLeftUpper(int X, int Y) override;
	void setRect(int X, int Y, int widthVal, int heightVal) override;

protected:
	int x;
	int y;
	int width;
	int height;
};
