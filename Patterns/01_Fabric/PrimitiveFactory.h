#pragma once
#include "IPrimitiveFactory.h"

class PrimitiveFactory : public IPrimitiveFactory
{
public:
	PrimitivePtr getEllipse() override;
	PrimitivePtr getRectangle() override;
	PrimitivePtr getLine() override;
};

