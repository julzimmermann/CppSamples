#pragma once
#include "IPrimitive.h"

class Line : public APrimitive
{
public:
	Line() = default;
	~Line() override = default;

	void Draw() override;
};

