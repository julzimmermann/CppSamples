file(GLOB SOURCES *.cpp)
file(GLOB HEADR *.h)
add_executable(FabricExe ${SOURCES} ${HEADR})
add_library(FabricLib STATIC ${SOURCES} ${HEADR})