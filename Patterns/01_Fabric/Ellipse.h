#pragma once
#include "IPrimitive.h"

class Ellipse : public APrimitive
{
public:
	Ellipse() = default;
	~Ellipse() override = default;

	void Draw() override;
};

