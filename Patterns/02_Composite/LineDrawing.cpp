#include <stdexcept>

#include "LineDrawing.h"

LineDrawing::LineDrawing(int X, int Y, int width, int height)
{
	setRect(X, Y, width, height);
}

void LineDrawing::DrawItem()
{
	Draw();
}

void LineDrawing::AddItem(DrawingItemPtr item)
{
	throw std::runtime_error("AddItem to Line not possible");
}

