#include <stdexcept>

#include "RectangleDrawing.h"

RectangleDrawing::RectangleDrawing(int X, int Y, int width, int height)
{
	setRect(X, Y, width, height);
}

void RectangleDrawing::DrawItem()
{
	Draw();
}

void RectangleDrawing::AddItem(DrawingItemPtr item)
{
	throw std::runtime_error("AddItem to Rectangle not possible");
}
