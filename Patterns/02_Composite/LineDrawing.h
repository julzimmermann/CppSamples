#pragma once
#include "../01_Fabric/Line.h"
#include "IDrawingItem.h"

class LineDrawing :
	public Line, public IDrawingItem
{
public:
	LineDrawing() = default;
	LineDrawing(int X, int Y, int width, int height);
	~LineDrawing() override = default;

	void DrawItem() override;
	void AddItem(DrawingItemPtr item) override;
};

