#include <stdexcept>

#include "EllipseDrawing.h"

EllipseDrawing::EllipseDrawing(int X, int Y, int width, int height)
{
	setRect(X, Y, width, height);
}

void EllipseDrawing::DrawItem()
{
	Draw();
}

void EllipseDrawing::AddItem(DrawingItemPtr item)
{
	throw std::runtime_error("AddItem to Ellipse not possible");
}
