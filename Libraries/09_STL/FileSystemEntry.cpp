#include "FileSystemEntry.h"


FileSystemEntry::FileSystemEntry() : Size(0), DirFlag(false)
{
}

FileSystemEntry::FileSystemEntry(const std::string Name, const long long Size, const bool dirFlag) 
	: Name(Name), Size(Size), DirFlag(dirFlag)
{
}

FileSystemEntry::~FileSystemEntry()
{
}

void FileSystemEntry::setName(const std::string name)
{
	Name = name;
}

void FileSystemEntry::setSize(const long long size)
{
	Size = size;
}

void FileSystemEntry::setDir(bool dirFlag)
{
	this->DirFlag = dirFlag;
}
