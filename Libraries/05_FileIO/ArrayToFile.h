#ifndef SAMPLES_ARRAYTOFILE_H
#define SAMPLES_ARRAYTOFILE_H

#include <array>
#include <ios>
#include <iostream>
#include <fstream>

template<typename T, size_t len>
class ArrayToFile {
public:
    explicit ArrayToFile(std::string name) : name(std::move(name)) {
    }

    bool WriteToFile(const std::array<T, len> &data) {
        auto writeResult = false;
        if (openFile(wrMode)) {
            writeResult = writeData(data);
            closeFile();
        }
        return writeResult;
    }

    bool ReadFromFile(std::array<T, len> &data) {
        auto readResult = false;
        if (openFile(rdMode)) {
            readResult = readData(data);
            closeFile();
        }
        return readResult;
    }

private:
    const static std::ios_base::openmode wrMode;
    const static std::ios_base::openmode rdMode;

    std::fstream file;
    std::string name;

    bool openFile(const std::ios_base::openmode &mode) {
        file.open(name, mode);
        return !file.bad();
    }

    void closeFile() {
        file.close();
    }

    bool writeData(const std::array<T, len> &data) {
        for (size_t i = 0; i < data.size(); i++) {
            try {
                file << data[i] << std::endl;
            }
            catch (...) {
                return false;
            }
        }
        return true;
    }

    bool readData(std::array<T, len> &data) {
        for (size_t i = 0; i < data.size(); i++) {
            try {
                file >> data[i];
            }
            catch (...) {
                return false;
            }
        }
        return true;
    }

};

template<typename T, size_t len> const std::ios_base::openmode ArrayToFile<T, len>::wrMode = std::ios::out | std::ios::in | std::ios::trunc;
template<typename T, size_t len> const std::ios_base::openmode ArrayToFile<T, len>::rdMode = std::ios::in;

#endif //SAMPLES_ARRAYTOFILE_H
