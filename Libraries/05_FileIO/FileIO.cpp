#include <iostream>
#include <iomanip>
#include <fstream>

#include "ArrayToFile.h"

#ifdef Boost_FILESYSTEM_FOUND
#include <boost/filesystem.hpp>
namespace filesystem = boost::filesystem;
#elif _MSC_VER
#include <filesystem>
namespace filesystem = std::experimental::filesystem::v1;
#else
#include <experimental/filesystem>
namespace filesystem = std::experimental::filesystem::v1;
#endif

using namespace std;

const char* FNAME = "Data.bin";
const int MaxValues = 100;

void WriteToFile()
{
	ofstream outfile(FNAME, ios::out | ios::binary);
	if (outfile)
	{
		for (int i = 0; i < MaxValues; i++)
		{
			try
			{
				int help = i * 10 + 1;
				outfile.write(reinterpret_cast<const char *>(&help), sizeof(int));
			}
			catch (...)
			{
				cerr << "could not write to " << FNAME << endl;
			}
		}
		outfile.close();
	}
	else
		cerr << "could not open " << FNAME << endl;
}

void ReadFromFile()
{
	ifstream infile(FNAME, ios::in | ios::binary);
	if (infile)
	{
		for (int i = 0; i < MaxValues; i += 10)
		{
			try
			{
				int help;
				infile.seekg(i * sizeof(int), ios::beg);
				infile.read(reinterpret_cast<char*>(&help), sizeof(int));
				cout << "Read Value" << setw(2) << i << "=" << help << endl;
			}
			catch (...)
			{
				cerr << "could not read from " << FNAME << endl;
			}
		}
		infile.close();
	}
	else
		cerr << "could not open " << FNAME << endl;
}

void KillFileIfExists(const char* name)
{
	try
	{
        filesystem::path myPath{ name };
		if (filesystem::exists(myPath))
            filesystem::remove(myPath);
	}
	catch (...)
	{
		cerr << "could not delete " << FNAME << endl;
	}
}

void UseClass()
{
	const auto textFileName = "Data.txt";
	KillFileIfExists(textFileName);

	array<double, 10> data = { 1.1, 2, 3, 4, 5, 6, 7, 8, 9, 10.1 };
	ArrayToFile<double, 10> adapter(textFileName);
	auto result = adapter.WriteToFile(data);
	if (result)
	{
		array<double, 10> readData;
		if (!adapter.ReadFromFile(readData))
			cout << "read data failed\n";
		else
		{
			for (const auto& value : readData)
				cout << value << endl;
		}
	}
	else
		cout << "write data failed\n";
}

int main()
{
	KillFileIfExists(FNAME);
	WriteToFile();
	ReadFromFile();

	UseClass();
}
